import { Box, Input, InputGroup, Text } from '@chakra-ui/react';
import Head from 'next/head'
import Image from 'next/image'
import Todo from '../components/Todo';
import styles from '../styles/Home.module.css'

export default function Home() {

    const todos = [
        {
            description: 'HStack',
            tasks: [
                {description: 'Iniciar a leitura dos artigos', checked: false}
            ]
        }
    ];

    return (
        <Box>
            <InputGroup py={4}>                
                <Input variant={'outline'} placeholder={'Add todo'} />                
            </InputGroup>
            {todos.map((todo, i)=><Todo key={i} todo={todo} />)}
        </Box>
    )
}
