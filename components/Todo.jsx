import { ChevronRightIcon, DeleteIcon } from "@chakra-ui/icons";
import { Box, Button, Checkbox, Divider, HStack, IconButton, Input, InputGroup, Text, VStack } from "@chakra-ui/react";
import { useEffect, useState } from "react";

function Task(task) {

    const { description, checked } = task;

    return (
        <HStack>
            <Text>{description}</Text>
            <Checkbox value={checked} />
        </HStack>
    )

}

export default function Todo({todo}) {

    const [task, setTask] = useState('');
    const [tasks, setTasks] = useState([...todo.tasks]);

    function addTask() {
        const nTask = {description: task, checked: false};
        setTasks([...tasks, nTask]);
        setTask('');
    }

    function onChange(value) {
        setTask(value);
    }

    return (
        <VStack gap={4} border={'1px'} borderColor={'#222222'} w={'3xl'} rounded={'md'} p={4}>
            <HStack w={'full'} justifyContent={'space-between'}>
                <Text fontSize={24}>{todo.description}</Text>
                <IconButton colorScheme={'red'} icon={<DeleteIcon />} />
            </HStack>
            <InputGroup gap={4} justifyContent={'center'}>                
                <Input value={task} onChange={({target})=>onChange(target.value)} w={'md'} variant={'flushed'} />
                <Button onClick={addTask}>Add task</Button>
            </InputGroup>      
            <VStack w={'full'} alignItems={'start'} pt={4}>
                <Box pl={10}>
                    {tasks.map((task, i)=><Task key={i} {...task}/>)}
                </Box> 
            </VStack>
        </VStack>
    )


}