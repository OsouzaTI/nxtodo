import { Box, Grid, Heading, HStack, Text, VStack } from "@chakra-ui/react";

export default function Layout({children}) {

    return (
        <VStack>
            <HStack color={'#fff'} justifyContent={'center'} w={'full'} h={'36'} bg={'#3c3c3c'}>
                <Heading>NXTodo</Heading>
            </HStack>
            {children}
        </VStack>
    )

}